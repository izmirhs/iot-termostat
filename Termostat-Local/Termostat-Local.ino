//#include <aJSON.h>
#include <ESP8266WiFi.h>
#include <ArduinoJson.h>


//AP definitions
#define AP_SSID "*******"
#define AP_PASSWORD "*********"


// EasyIoT server definitions
#define EIOT_IP_ADDRESS  "192.168.0.2"
#define EIOT_PORT        80

String line;
const int pin = 2;
void wifiConnect()
{
    Serial.print("Connecting to AP");
    WiFi.begin(AP_SSID, AP_PASSWORD);
    //WiFi.config(ip, gateway, subnet);
    while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }
  
  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println(WiFi.localIP());
}


void setup() {
  Serial.begin(115200);
  
  wifiConnect();
  pinMode(pin, OUTPUT);
  digitalWrite(pin, !LOW);
    
}

void sendTemperature()
{  
   WiFiClient client;
   
   while(!client.connect(EIOT_IP_ADDRESS, EIOT_PORT)) {
    Serial.println("connection failed");
    wifiConnect(); 
  }
  String clientName;
  uint8_t mac[6];
  WiFi.macAddress(mac);
  for(int i=0;i<6;i++){
    clientName += String(mac[i], 16);
  }
  String url = "http://192.168.0.2";
  url += "/test.php?id="+clientName+"&kod=1"; // generate EasIoT server node URL

  Serial.print("POST data to URL: ");
  Serial.println(url);
  
  client.print(String("POST ") + url + " HTTP/1.1\r\n" +
               "Host: " + String(EIOT_IP_ADDRESS) + "\r\n" + 
               "Connection: close\r\n" + 
               "Authorization: Basic\r\n" + 
               "Content-Length: 0\r\n" + 
               "\r\n");

  delay(100);
    while(client.available()){
    line = client.readStringUntil('\r');
    //Serial.print(line);
    /*
    char json_data[100];
    line.toCharArray(json_data, 100);
    aJsonObject* jsonObject = aJson.parse(json_data);
    aJsonObject* name = aJson.getObjectItem(jsonObject , "ID");
    Serial.println(name->valuestring);
     */
  }
  
  
  Serial.println(line);
  StaticJsonBuffer<200> jsonBuffer;
  
      char json[line.length()];
      int i = 0;
      while (i <= line.length()-1) {
        json[i] = line.charAt(i);
        i++;
      }
      
      //line.toCharArray(json, 100);
      JsonObject& root = jsonBuffer.parseObject(json);
      if (!root.success()) {
      Serial.println("parseObject() failed");
      return;
      }      
      //const char* id = root["ID"]; //ORG
      String id=root["ID"];
      int kod=root["KOD"];
      int modu=root["MODU"]; 
      Serial.println(id);
      Serial.println(kod);
      Serial.println(modu);

      if(id==clientName && kod==1 && modu==1){
        
        digitalWrite(pin, !HIGH);
        }else{
        digitalWrite(pin, !LOW); 
      }
      Serial.println("Connection closed");
}

void loop() {

    sendTemperature();
  
}




