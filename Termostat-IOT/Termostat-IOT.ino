#include <ESP8266WiFi.h>
#include <MQTT.h>
#include <EEPROM.h>

#define DEBUG

#define AP_SSID     "ssid"
#define AP_PASSWORD "pass" 

#define EIOTCLOUD_USERNAME "username"
#define EIOTCLOUD_PASSWORD "password"


#define EIOT_CLOUD_ADDRESS "cloud.iot-playground.com"

//iot-playground üzerinde kullanımımıza uygun modülleri ekliyor ve
//bunların topic değerlerini kullanım amacına göre isimlendiriyoruz

#define temptopic "/1/Sensor.Parameter1"
#define buttopic  "/2/Sensor.Parameter1"
#define settemptopic  "/3/Sensor.Parameter1"
#define pushbuttontopic "/4/Sensor.Parameter1"
#define analogout "/3/Sensor.Parameter1"


#define pushbuttonduration  1000

#define APMODE_SSID   "izmirhs_iot"
#define APMODE_PASS   "izmirhs"

const int buttonPin = 0; 
const int outPin = 16;  //nodemcu
//const int outPin = BUILTIN_LED;  //ESP8266-01
const int ee_res_pin = 0;

//MQTT nesnesi yarat
MQTT myMqtt("", EIOT_CLOUD_ADDRESS, 1883);

// ugyulamaya özel fonksiyon prototipleri
String macToStr(const uint8_t* mac);
void myConnectedCb();
void myDisconnectedCb();
void myPublishedCb();
void myDataCb(String& topic, String& data);
int storeAPinfo(String ssid, String pass);
int readAPinfo(char *ssid, char *pass);
int checkMode(int pin, bool state, int duration);
void AP_Setup(void);
void AP_Loop(void);
void(* resetFunc) (void) = 0; //declare reset function @ address 0

//değişkenler
bool stepOk = false;
int buttonState;

boolean result;
boolean switchState = false;
boolean pushbutton = false;
String valueStr(switchState);
String instanceId = "";
int lastButtonState = LOW;
int temp= 0;
int settemp = 10;
int timer, buttontimer;
int multiplier = 2;
char tempstr[16];
int tempstrindex = 0;


char ap_ssid[16];
char ap_pass[16];

WiFiServer server(80);

void setup()
{ 
 Serial.begin(9600);

  EEPROM.begin(512);
  while (!Serial) {
    ; // Seri portun açılmasını bekle
  }
  switchState = false; 
  delay(150);
  pinMode(buttonPin, INPUT);
  pinMode(buttonPin, INPUT_PULLUP);
  pinMode(outPin, OUTPUT);
 
  Serial.println("Mode testi basliyor");
  delay(2000);
 
  if(0 == checkMode(pushbutton, false, 3000)){
    Serial.println("AP mode devrede");
    AP_Setup();
    AP_Loop();
    resetFunc();
  }

//  else if( !readAPinfo(ap_ssid, ap_pass)){
//
//    #ifdef DEBUG
//    Serial.println("32byte of eeprom:");
//    for(int i = 0; i< 32; i++){
//      Serial.write( EEPROM.read(i));
//    }
//    Serial.println("Eepromdan oku");
//    Serial.print("eeprom ssid:");
//    Serial.println(ap_ssid);
//    Serial.print("eeprom_pass:");
//    Serial.println(ap_pass);
//    #endif   
//    WiFi.begin(ap_ssid, ap_pass);
//  }
 
  else{
    WiFi.begin(AP_SSID, AP_PASSWORD);
    Serial.println("connecting to default");
  }
 
  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
    Serial.print(".");
  }
  #ifdef DEBUG
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println("Connecting to MQTT server");
  #endif
 
  // tekil modül IDsi yarat. MAC adresin ardına işlemci açılış sonrası geçen us nin 1 byteı
  String clientName;
  uint8_t mac[6];
  WiFi.macAddress(mac);
  clientName += macToStr(mac);
  clientName += "-";
  clientName += String(micros() & 0xff, 16);
  myMqtt.setClientId((char*) clientName.c_str());

  #ifdef DEBUG
  Serial.print("MQTT client id:");
  Serial.println(clientName);
  #endif

  // MQTT eventleri için fonksiyonların tanımlanması
  myMqtt.onConnected(myConnectedCb);
  myMqtt.onDisconnected(myDisconnectedCb);
  myMqtt.onPublished(myPublishedCb);
  myMqtt.onData(myDataCb);

   // Kullanıcı adı ve şifre ayarla, Sunucuya bağlan
  myMqtt.setUserPwd(EIOTCLOUD_USERNAME, EIOTCLOUD_PASSWORD); 
  myMqtt.connect();

  delay(500);
 
  //publish switch state
  result = myMqtt.publish(buttopic, valueStr);
  myMqtt.subscribe(buttopic);
  myMqtt.subscribe(settemptopic);
  myMqtt.subscribe(pushbuttontopic);
  myMqtt.subscribe(analogout, 1);

  valueStr = "25";
  result = myMqtt.publish(temptopic, valueStr);

  switchState = false;
  lastButtonState = digitalRead(buttonPin);
}

void loop() {
  while (WiFi.status() != WL_CONNECTED) {
    delay(200);
    Serial.print(".");
  }

  if(pushbutton == false){
   if(millis() - buttontimer > pushbuttonduration){
    valueStr = String(pushbutton);
    pushbutton = true;
    Serial.println("KAPA");
    digitalWrite(outPin, pushbutton);
    myMqtt.publish(pushbuttontopic, valueStr );   
    delay(200);
   }
  }
 
  int reading = digitalRead(buttonPin);

  if (reading != lastButtonState) {
    if (reading == LOW)
    {
      switchState = !switchState;
      valueStr = String(switchState);
      Serial.print(valueStr);
      result = myMqtt.publish(buttopic, valueStr);
      delay(100);    
    }
    lastButtonState = reading;
    digitalWrite(outPin, !switchState);
  }
}

String macToStr(const uint8_t* mac)
{
  String result;
  for (int i = 0; i < 6; ++i) {
    result += String(mac[i], 16);
    if (i < 5)
      result += ':';
  }
  return result;
}


/*
 *
 */
void myConnectedCb() {
#ifdef DEBUG
  Serial.println("connected to MQTT server");
#endif
}

void myDisconnectedCb() {
#ifdef DEBUG
  Serial.println("disconnected. try to reconnect...");
#endif
  delay(100);
  myMqtt.connect();
}

void myPublishedCb() {
#ifdef DEBUG 
  Serial.println("published.");
#endif
}

void myDataCb(String& topic, String& data) { 
//  Serial.print(topic);
//  Serial.print(": ");
//  Serial.println(data);
 
   if (topic == buttopic)
  {
    switchState = (data == String("1"))? true: false;
//    digitalWrite(outPin, !switchState);
  }
  else if (topic == analogout)
  {
    Serial.println(data);
    
  }
 
  else if (topic == pushbuttontopic)
  {
    if(data == String("1"))
    {
      pushbutton = false;
      buttontimer = millis();
      valueStr = data;
      digitalWrite(outPin, pushbutton);
      Serial.println("AC");
      myMqtt.publish(pushbuttontopic, valueStr);
    }
  }
}

void AP_Setup(void){
  Serial.println("setting mode");
  WiFi.mode(WIFI_AP);
  // Do a little work to get a unique-ish name. Append the
  // last two bytes of the MAC (HEX'd) to "Thing-":
  uint8_t mac[WL_MAC_ADDR_LENGTH];
  Serial.println("setting mac");
  WiFi.softAPmacAddress(mac);
  Serial.println("starting ap");
  WiFi.softAP(APMODE_SSID, APMODE_PASS);
  Serial.println("running server");
  server.begin();
}

void AP_Loop(void){

  bool  inf_loop = true;
  int  val = 0;
  WiFiClient client;

  Serial.println("AP loop");

  while(inf_loop){
    while (!client){
      Serial.print(".");
      delay(100);
      client = server.available();
    }
    String ssid;
    String passwd;
    // Read the first line of the request
    String req = client.readStringUntil('\r');
    client.flush();

    // Prepare the response. Start with the common header:
    String s = "HTTP/1.1 200 OK\r\n";
    s += "Content-Type: text/html\r\n\r\n";
    s += "<!DOCTYPE HTML>\r\n<html>\r\n";

    if (req.indexOf("&") != -1){
      int ptr1 = req.indexOf("ssid=", 0);
      int ptr2 = req.indexOf("&", ptr1);
      int ptr3 = req.indexOf(" HTTP/",ptr2);
      ssid = req.substring(ptr1+5, ptr2);
      passwd = req.substring(ptr2+10, ptr3);   
      Serial.println(ssid);
      Serial.println(passwd);
      val = -1;
    }

    if (val == -1){
      storeAPinfo(ssid, passwd);
      s += "Ayar Tamam";
      s += "<br>"; // Go to the next line.
      s += "Simdi Yeniden Baslatiliyor";
      inf_loop = false;
    }

    else{
      String content="";
      // output the value of each analog input pin
      content += "<form method=get>";
      content += "<label>SSID</label><br>";
      content += "<input  type='text' name='ssid' maxlength='30' size='15'><br>";
      content += "<label>Sifre</label><br>";
      content += "<input  type='text' name='password' maxlength='30' size='15'><br><br>";
      content += "<input  type='submit' value='Kaydet' >";
      content += "</form>";
      s += content;
    }
   
    s += "</html>\n";
    // Send the response to the client
    client.print(s);
    delay(1);
    client.stop();
  }
}

int storeAPinfo(String ssid, String pass){
  int i = 0;
  int ee_index = 0;
 
  EEPROM.write(ee_index++, 0x01);
  EEPROM.write(ee_index++, (byte)ssid.length());

  Serial.print("WE2 SSID");
  for (i=0; i<ssid.length(); i++){
    EEPROM.write(ee_index++, ssid.charAt(i));
    Serial.write(ssid.charAt(i));
  }
   
  EEPROM.write(ee_index++, 0x02);
  EEPROM.write(ee_index++, (byte)pass.length());

  Serial.println("WE2 PASS");
  for (i=0; i<pass.length(); i++){
    EEPROM.write(ee_index++, pass.charAt(i));
    Serial.write(pass.charAt(i));
  }
 
  EEPROM.commit();
  return 0;
}

int readAPinfo(char *ssid, char *pass){

  char ch;
  int slen, plen, i = 0;
  int ee_index = 0;
  int ret = 0;

  ch = EEPROM.read(0);
  if(ch == 0x01){
    slen = EEPROM.read(1);
    for(i = 0; i< slen; i++){
      *ssid++ =  EEPROM.read(2+i);
    }
    *ssid = '\0';

    ch = EEPROM.read(2 + slen);
    if(ch == 0x02){
      plen = EEPROM.read(3+slen);
      for(i = 0; i< plen; i++){
        *pass++ =  EEPROM.read(4+slen+i);
      }
      *pass = '\0';
    }
    else ret = -2;
  }
  else ret = -1;

  return ret;
}

int checkMode(int pin, bool state, int duration){
  int ret = 0;
  int cnt = 0;
  while(cnt < duration){
    if(state != digitalRead(pin)){
      ret = -1;
      break;
    }
    else delay(100);
    cnt += 100;
  }
  return ret;
}


